output "network" {
  value = {
    vpc = module.network.vpc_id
    gw_subnet = module.network.gateway_subnet_ids
    db_subnet = module.network.db_subnet_ids
    app_subnet = module.network.app_subnet_ids
  }
}

output "eks" {
  value = {
    kubeconfig = module.eks.eks_kubeconfig
    target_group_arn = module.eks.target_group_arn
    node_sg_id = module.eks.node_sg_id
  }
}

output "security_group" {
  value = {
    master = module.security_groups.sg_master
    node = module.security_groups.sg_node
  }
}