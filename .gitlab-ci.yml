
default:
  cache:
    paths:
      - /go/pkg/mod
    # I don't like using the cache for .terraform because it will cause residual problems should you make changes.
    #    - .terraform

stages:
  - init
  - validate
  - plan
  - lint
  - unittest
  - test
  - terratest
  - apply
  - destroy

.terraform:
  image:
    #name: gitlab.com/sandlin/dependency_proxy/containers/hashicorp/terraform:light
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

tf init:
  stage: init
  extends: .terraform
  script:
    - terraform init -backend-config="username=${GITLAB_USERNAME}" -backend-config="password=${GITLAB_TOKEN}" -var-file=./config/production.tfvars
  artifacts:
    paths:
      - .terraform


tf validate:
  stage: validate
  extends: .terraform
  script:
    - terraform validate


# We must strip creds out of the json file before uploading to GitLab
# https://docs.gitlab.com/ee/user/infrastructure/#output-terraform-plan-information-into-a-merge-request
tf plan:
  stage: plan
  extends: .terraform
  script:
    - apk --no-cache add jq
    - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
    - terraform plan -var-file=./config/production.tfvars -out=tfplan.tfplan
    - terraform show -json tfplan.tfplan | convert_report > tfplan.json
  artifacts:
    name: plan
    paths:
      - tfplan.tfplan
    reports:
      terraform: tfplan.json
    expire_in: 1 month

#
# This will fail at the end
# module.eks.kubernetes_config_map.aws_auth: Still creating... [30s elapsed]
# Error: Post "https://D75952DDEE74C753866C01BB34CD673C.gr7.us-east-1.eks.amazonaws.com/api/v1/namespaces/kube-system/configmaps": dial tcp 54.157.202.110:443: i/o timeout
#   on modules/eks/allow_nodes.tf line 24, in resource "kubernetes_config_map" "aws_auth":
#   24: resource "kubernetes_config_map" "aws_auth" {
# This is a known issue where the EKS API does not come up immediately and there is a delay between the creation completing, and the API actually being available. This is inconsistent, so try again to work around this.
#
tf apply:
  stage: apply
  extends: .terraform
  script:
    - terraform apply -input=false tfplan.tfplan
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual

tf destroy:
  stage: destroy
  extends: .terraform
  script:
    - terraform destroy -var-file=./config/production.tfvars -auto-approve
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual

include:
  - local: 'modules/.gitlab-ci-modules.yml'
  - template: Code-Quality.gitlab-ci.yml
