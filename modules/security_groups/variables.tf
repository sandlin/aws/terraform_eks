
variable "cluster_name" {
  description = "name of AWS cluster"
}
variable "vpc_id" {
  description = "The VPC in which to apply these SGs"
}
variable "accessing_computer_ip" {
  description = "The IP of your work computer, which will have access to the k8s cluster & port 22"
}
variable "region" {
  description = "AWS region"
}