
output "app_subnet_ids" {
  value       = aws_subnet.app.*.id
  description = "The app subnet ids"
}

output "db_subnet_ids" {
  value       = aws_subnet.db.*.id
  description = "The db subnet ids"
}

output "gateway_subnet_ids" {
  value       = aws_subnet.gateway.*.id
  description = "The gateway subnet ids"
}

output "vpc_id" {
  value = aws_vpc.vpc.id
  description = "The VPC ID"
}
