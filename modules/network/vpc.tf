
# https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform_part2/
resource "aws_vpc" "vpc" {
  cidr_block = "69.0.0.0/16"

  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Owner = var.user
    Name = var.vpc_name
    "kubernetes.io/cluster/${var.vpc_name}" = "shared"
  }
}
