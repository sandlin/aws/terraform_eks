
# src: https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform-part-iv
output "eks_kubeconfig" {
  value = local.kubeconfig
  depends_on = [
    aws_eks_cluster.eks_cluster
  ]
}
output "target_group_arn" {
  value = aws_lb_target_group.tf_eks.arn
}

output "node_sg_id" {
  value = aws_security_group.eks_node.id
}