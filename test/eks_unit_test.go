/*
* This is being done as a POC with everything in GO, as this is the language Terraform is written in AND
*     we have terratest.
* That said, most sysadmins will use Python more than GO and the testing I'm doing here would be half the length and
*     far less complicated if done in Python.
*/
package test
// REF https://www.infralovers.com/en/articles/2019/12/18/terraform-with-terratest-in-gitlab-pipeline/
import (
	"encoding/json"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"os"
	"testing"
)

func getJsonMap(m map[string]interface{}, key string) map[string]interface{} {
	raw := m[key]
	sub, ok := raw.(map[string]interface{})
	if !ok {
		return nil
	}
	return sub
}
func TestUnitEksSetup(t *testing.T) {
	t.Parallel()

	region := "us-east-1"
	os.Setenv("AWS_DEFAULT_REGION", region)
	//testUser := fmt.Sprint("eks_test", randVal)
	testClusterName := "ekstest"
	vpcId := "vpc-06af31c7fdb7b7a4f"
	//subnetCount := "1"
	appSubnetIds := []string{"subnet-04580cf1e2d0adc01", "subnet-0b154648b3323bbb6"}
	accessing_computer_ip := "97.120.179.43"
	tfOptions := &terraform.Options{
		TerraformDir: "../modules/eks",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			//"user": testUser,
			"cluster_name": testClusterName,
			"vpc_id": vpcId,
			"region": region,
			"accessing_computer_ip": accessing_computer_ip,
			//"subnet_count": subnetCount,
			"app_subnet_ids": appSubnetIds,

		},
	}
	// init and plan
	tfPlanOutput := "terraform.tfplan"
	terraform.Init(t, tfOptions)
	terraform.RunTerraformCommand(t, tfOptions, terraform.FormatArgs(tfOptions, "plan", "-out="+tfPlanOutput)...)
	tfOptions.Vars = nil

	// read the plan as json
	jsonplan, err := terraform.RunTerraformCommandAndGetStdoutE(t, tfOptions, terraform.FormatArgs(tfOptions, "show", "-json", tfPlanOutput)...)
	jsonMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(jsonplan), &jsonMap)
	if err != nil {
		panic(err)
	}
	plannedValues := getJsonMap(jsonMap, "planned_values")
	rootModule := getJsonMap(plannedValues, "root_module")
	print(rootModule)

	/*
	 * Now this is way too much stuff to have to do BEFORE we even start testing.
	 * Python would have been FAR better here.
	 */
}
