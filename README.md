# Terraform Module to create EKS cluster.

NOTE: This is a WIP, in the since it is not yet a standardized, reusable Terraform module. Currently, this project creates a cluster in AWS, to be used as an example.

---

[[_TOC_]]

---

## Description
This project is a small piece of an effort to show how we can manage Infrastructure as Code via GitLab. IoC automation shares some core goals with ITIL's Service Transition: traceability, repeatability, and reportability. In order to achieve these three goals, everything is done via automation pulled from version control.

User specific tokens, certificates, etc. will be stored in files outside version control, except when using encrypted files such as Ansible Vault, or credential management systems such as Hashicorp Vault.

Functionally, this project creates a network with required security groups and an EKS in AWS.

---

### Overview
There are 3 Terraform modules: `eks`, `network`, & `security_groups`, and one Terraform file to manage the creation `create_eks.tf`

The variable values for each environment are stored in the `config` directory.

The `config/production.tfvars` stores the values to be used by default (when executed as part of the CI job)

For an example of how you may run this setup, see `example_run.sh`. NOTE: Create and Delete are commented out to prevent accidental runs.

---

### GitLab integration
This project uses GitLab features for:
1. Version Control
1. Terraform State Storage
1. Terraform Change Reporting in Merge Requests
1. CI/CD Automation - Based upon the source of the commit (feature branch, MR, master), a different subset of stages will execute:

  |    Job | Feature Branch  | Merge Request  |  Master|
  |    ---|----|----|---|
  |    TF Init  | x  | x | x |
  |    TF Validate  | x  | x | x |
  |    TF Plan  | x  | x | x |
  |    Lint (per module) | x | x  | x |
  |    Unit Test (per module)  | x  | x | x |
  |    Code Quality  | x  | x  | x |
  |    Integration Test (per module)| | x | x|
  |    TF Apply  |   |   | x |



---

### Requirements
#### Local
1. Install Terraform
1. Install AWS CLI
   1. With this, you will create a file called `$HOME/.aws/credentials`
      ```
      [default]
      aws_access_key_id = <The access key ID you get from AWS>
      aws_secret_access_key = <The secret access key from AWS>
      ```
1. Create a file called `$HOME/.aws/.tfvars`
    ```
    #AWS Account ID
    aws_account_id = "VALUE WILL COME FROM https://gitlab.com/groups/<YOURGROUP>/aws/-/clusters/new?provider=aws"

    #AWS External ID
    aws_external_id = "VALUE WILL COME FROM https://gitlab.com/groups/<YOURGROUP>/aws/-/clusters/new?provider=aws"
    ```

1. GitLab [Personal Access Token](https://gitlab.com/profile/personal_access_tokens) with `api` scope\
   Be sure to copy the value of this token when created, as it will no longer be accessible

1. Create a file called `$HOME/.gitlab/.creds`
      ```
      username="${your_gitlab_username}"
      password="${your_gitlab_token}"
      ```

#### GitLab
1. Under `CI/CD Settings`, create the following `Variables`. The values will come from the files created above. \
   Ensure you select `Mask variable` so the values are hidden during execution.

   1. Create the variables required for GitLab to maintain the state of your TF environment
   | Key | Value |
   | ---|--- |
   | GITLAB_TOKEN  | ${your_gitlab_username}  |
   | GITLAB_USERNAME | ${your_gitlab_token} |
   
   1. Create the variables required for GitLab to execute the AWS CLI calls.
   | Key | Value |
   | ---|--- |
   | AWS_ACCESS_KEY_ID| ${aws_access_key_id} |
   | AWS_SECRET_ACCESS_KEY  |  ${aws_secret_access_key} |


---
### Terraform State Management Initialization

Refs:
- [GitLab Ref](https://docs.gitlab.com/ee/user/infrastructure/#infrastructure-as-code-with-gitlab-managed-terraform-state)
- [Terraform Ref](https://www.terraform.io/docs/backends/config.html)

This is where your `~/.gitlab/.creds` file is utilized.

#### Variables
Use `example_run.sh` for a reference. Modify the variables being passed.

#### TF Backend
There are several ways to store your [Terraform backend](https://www.terraform.io/docs/backends/config.html).

I used the [GitLab instructions](https://docs.gitlab.com/ee/user/infrastructure/#infrastructure-as-code-with-gitlab-managed-terraform-state) as a ref and moved most of the data into `backend` variables. This helps ensure individual configurations do not influence the outcome by storing the incorrect state on someone's system.

The only data left out is the GitLab `username` and `password` (Personal Access Token), which is stored in `~/.gitlab/.creds`

---
#### Testing
Each module utilizes [Terratest](https://github.com/gruntwork-io/terratest) for unit & integration testing.
In order to execute tests locally, you must first initialize the Go tests.
```
cd test
go mod init test
```
At this point, you can execute tests in the test directory via `go test <test module>`:
```
cd test
go test network_int_test.go
```


### Related GitLab Stages

- [ ] Manage

- [ ] Plan

- [x] Create

- [x] Verify

- [ ] Package

- [ ] Secure

- [x] Release

- [x] Configure

- [ ] Monitor

- [ ] Defend

### GitLab Use-Cases
Add any use-cases to how GitLab can intersect with this topic.

 - Prospect is looking to utlize GitLab for SDLC of Terraform modules
 - Prospect is looking to validate Terraform changes before they are rolled out to environments.
 - Prospect is looking to utilize GitLab for Terraform State Management.


### Links to Demos

### Links to Trainings
(udemy courses that are validated)

### Customer conversations related to this topic (Chorus or Zoom recordings)
(Links to any recordings that would be beneficial to watch on this topic)

### Links to slides / diagrams to utilize in customer conversations
(Related slides or diagrams that have been used to articulate the value of GitLab with this use case)

### Links to External Resources (Blog posts, Presentation/Demo videos, Tutorials)



# NOTE: This project is also in [Communities of Practice](https://gitlab.com/gitlab-com/customer-success/communities-of-practice/terraform_eks)

