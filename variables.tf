
variable "user" {
  description = "Who are you?"
}
variable "cluster_name" {
  description = "name of AWS cluster"
}
variable "vpc_name" {
  description = "name of vpc"
}
variable "accessing_computer_ip" {
  type = string
}
variable "subnet_count" {
  description = "Number of subnets"
  type = number
}
variable "region" {
  description = "AWS region"
  default = "us-east-1"
}
variable "gitlab_namespace" {
  description = "namespace for GitLab's Tiller"
  default = "gitlab-managed-apps"
}
variable "aws_account_id" {
  type = string
}
variable "aws_external_id"{
  type = string
}
variable "gitlab_token" {
  type = string
}

//variable "keypair-name" {
//  type = "string"
//  description = "Name of the keypair declared in AWS IAM, used to connect into your instances via SSH."
//}
//output "eks_kubeconfig" {
//  value = local.kubeconfig
//  depends_on = [
//    aws_eks_cluster.master_cluster
//  ]
//}
